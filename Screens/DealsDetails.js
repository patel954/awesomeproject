import * as React from 'react';
import { View, Text, StyleSheet, Image} from 'react-native';
import {Container, Header, Content} from 'native-base';

class DealsDetails extends React.Component{

    constructor(props){
        super(props)
    }

    render(){

        const {item} = this.props.route.params

        return(
            <Container>
                <Content>
                    <Text style={styles.mainHeading}>{item.companyName}</Text>
                    <Image style={styles.imageContainer} source={item.imageUrl} />
                    <View style={styles.hr}>
                        <View style={styles.inHr}>
                            <Text style={{fontWeight:'bold'}}>Posted By:</Text>
                            <Text>{item.postedBy}</Text>
                        </View>
                        <View style={styles.inHr}>
                            <Text style={{fontWeight:'bold'}}>Offer valid till:</Text>
                            <Text>{item.offerValidity}</Text>
                        </View>
                        <View style={styles.inHr}>
                            <Text style={{fontWeight:'bold'}}>Location</Text>
                        </View>
                    </View>
                    <View style={{borderTopWidth: 1, borderTopColor: 'black', marginTop: 5}}>
                        <Text style={styles.subHeading}>Description</Text>
                        <Text>{item.description}</Text>
                    </View>
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    mainHeading:{
        fontSize: 30,
        fontWeight: 'bold',
        margin: '2%'
    },
    imageContainer:{
        margin: '5%',
        width: '90%',
        height: '100%'
    },
    subHeading:{
        fontSize: 25,
        fontWeight: 'bold',
        margin: '2%'
    },
    hr:{
        borderWidth: 1,
        borderTopColor: 'black',
        borderBottomColor: 'black',
        padding: '2%',
        flex: 1,
        flexDirection: 'row'
    },
    inHr:{
        width: '33.33%',
        height: '100%',
        alignItems: 'center'
    }
});

export default DealsDetails;