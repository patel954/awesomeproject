import * as React from 'react';
import { Text} from 'react-native';
import {  Button, Container, Header, Content, Right, Left } from 'native-base';
import { Body, Title } from 'native-base';

class AccountSettingsScreen extends React.Component{
    render(){
        return(
            <Container>

                <Header>

                    <Left>
                        <Button onPress={() =>
                            this.props.navigation.goBack()}>
                            <Text>Back</Text>
                        </Button>
                    </Left>

                    <Body style={{flex:1, alignItems:'center', justifyContent: 'center'}}>
                        <Title>Home</Title> 
                    </Body>

                    <Right>
                    </Right>
                </Header>

                <Content contentContainerStyle={{
                    flex:1,
                    alignItems: 'center',
                    justifyContent: 'center'
                }}>
                    <Text>Home Screen</Text>
                </Content>
            </Container>
        );
    }
}

export default AccountSettingsScreen;