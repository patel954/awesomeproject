import * as React from 'react';
import { View, Text, StyleSheet, FlatList, Image, TouchableOpacity } from 'react-native';
import { Button, Container, Header, Content, Right, Left, Item } from 'native-base';
import { Body, Title } from 'native-base';

import { createStackNavigator } from '@react-navigation/stack';

import FilterScreen from './FilterScreen';
import DealsDetails from './DealsDetails';

//const dataList =[{ key: 'a' },{ key: 'b' }, { key: 'c' }, { key: 'd' }, { key: 'e' }, { key: 'f' }, { key: 'g' }, { key: 'h' }, { key: 'i' }, { key: 'j' }, { key: 'jk' }];
const numColumns = 2;

const Stack = createStackNavigator();

const flatListData = [
    {
        "key": "1",
        "companyName": "Best Buy",
        "offerValidity": "12th March",
        "imageUrl": require("./../Images/airpods.jpg"),
        "postedBy": "Vishwa Shah",
        "description": "A product description is the marketing copy that explains what a product is and why it’s worth purchasing.A product description is the marketing copy."
    },
    {
        "key": "2",
        "companyName": "Best Buy",
        "offerValidity": "12th March",
        "imageUrl": require("./../Images/android.jpeg"),
        "postedBy": "Vishwa Shah",
        "description": "A product description is the marketing copy that explains what a product is and why it’s worth purchasing. The purpose of a product description is to."
    },
    {
        "key": "3",
        "companyName": "Best Buy",
        "offerValidity": "12th March",
        "imageUrl": require("./../Images/beats.jpg"),
        "postedBy": "Vishwa Shah",
        "description": "A product description is the marketing copy that explains what a product is and why it’s worth purchasing. The purpose of a product description is to."
    },
    {
        "key": "4",
        "companyName": "Best Buy",
        "offerValidity": "12th March",
        "imageUrl": require("./../Images/bose.jpg"),
        "postedBy": "Vishwa Shah",
        "description": "A product description is the marketing copy that explains what a product is and why it’s worth purchasing. The purpose of a product description is to."
    },
    {
        "key": "5",
        "companyName": "Best Buy",
        "offerValidity": "12th March",
        "imageUrl": require("./../Images/earphone.jpg"),
        "postedBy": "Vishwa Shah",
        "description": "A product description is the marketing copy that explains what a product is and why it’s worth purchasing. The purpose of a product description is to."
    },
    {
        "key": "6",
        "companyName": "Best Buy",
        "offerValidity": "12th March",
        "imageUrl": require("./../Images/iphone.jpg"),
        "postedBy": "Vishwa Shah",
        "description": "A product description is the marketing copy that explains what a product is and why it’s worth purchasing. The purpose of a product description is to."
    },
    {
        "key": "7",
        "companyName": "Best Buy",
        "offerValidity": "12th March",
        "imageUrl": require("./../Images/laptop.jpg"),
        "postedBy": "Vishwa Shah",
        "description": "A product description is the marketing copy that explains what a product is and why it’s worth purchasing. The purpose of a product description is to."
    },
    {
        "key": "8",
        "companyName": "Best Buy",
        "offerValidity": "12th March",
        "imageUrl": require("./../Images/ps4.png"),
        "postedBy": "Vishwa Shah",
        "description": "A product description is the marketing copy that explains what a product is and why it’s worth purchasing. The purpose of a product description is to."
    },
    {
        "key": "9",
        "companyName": "Best Buy",
        "offerValidity": "12th March",
        "imageUrl": require("./../Images/tv.jpeg"),
        "postedBy": "Vishwa Shah",
        "description": "A product description is the marketing copy that explains what a product is and why it’s worth purchasing. The purpose of a product description is to."
    }
];

//<Text style={styles.itemtext}>{item.key}</Text>
function DealsViewScreen({navigation}) {

    formatData = (dataList, numColumns) => {
        const totalRows =  Math.floor(dataList.length / numColumns)
        let totalLastRow = dataList.length - (totalRows * numColumns)

        while( totalLastRow !== 0 && totalLastRow !== numColumns){
            dataList.push({key: 'blank', empty: true})
            totalLastRow++
        }

        return dataList
    }

    _renderItem= ({item, index})=>{
        let {itemStyle, itemText, itemInvisible, itemTextHeader} = styles

        if(item.empty){
            return <View style={[itemStyle, itemInvisible, itemText, itemTextHeader]} />
        }
        return(
            <TouchableOpacity
            style={{height:'100%', width:'50%'}}
            onPress={() => navigation.navigate('DealsDetails', { item })}
            >
            <View style={itemStyle}>
                <Text  style={itemTextHeader}>{item.companyName}</Text>
                <Text style={itemText}>{item.offerValidity}</Text>
                <Image 
                    style={styles.imageContainer}
                    source={item.imageUrl}
                />
                <Text style={itemText}>Posted by:</Text>
                <Text>{item.postedBy}</Text>
            </View>
            </TouchableOpacity>
        )
    }


        return (
            <Container style={{backgroundColor:'lightgrey'}}>
                <Header >
                    <Left style={{marginBottom: 10}}>
                        <Button style={styles.imageButton}
                        onPress={() => navigation.navigate('Filter')}
                        >
                            <Text>Filter</Text>
                        </Button>
                    </Left>
                    <Body style={{ flex: 1, alignItems: 'center', justifyContent: 'center', marginBottom: 10}}>
                        <Title>Buy-The-Way</Title>
                    </Body>

                    <Right style={{marginBottom: 10}}>
                        <Button 
                            style={styles.imageButton}
                            onPress={() => navigation.openDrawer()}>
                                <Text>Menu</Text>
                        </Button>
                    </Right>
                </Header>

                <Content contentContainerStyle={{
                    flex: 1
                }}>
                    <View>
                        <FlatList
                            data={this.formatData(flatListData, numColumns)}
                            renderItem={ this._renderItem }
                            keyExtractor={(item, index) => index.toString()}
                            numColumns= {numColumns}
                        />
                    </View>
                </Content>
            </Container>
        );
  }

class DealsScreen extends React.Component {
    render(){
        return(
            <Stack.Navigator>
                <Stack.Screen name="Deals" component={DealsViewScreen} options={{ headerShown: false }} />
                <Stack.Screen name="Filter" component={FilterScreen} />
                <Stack.Screen name="DealsDetails" component={DealsDetails} options={{ title: 'Buy-The-Way' }} />
            </Stack.Navigator>
        );
    }
}


const styles = StyleSheet.create({
    itemStyle:{
        backgroundColor: 'white',
        height: 300,
        flex: 1,
        margin: 2,
        paddingLeft: '2%'
    },
    itemTextHeader:{
        color: 'black',
        fontSize: 24,
        fontWeight: 'bold'
    },
    itemtext:{
        color: 'black',
        fontSize: 5
    },
    itemInvisible:{
        backgroundColor: 'transparent'
    },
    imageContainer:{
        marginTop: '10%',
        marginLeft: '8%',
        marginBottom: '10%',
        width: '80%',
        height: '60%'
    },
    imageButton:{
        backgroundColor: 'transparent'
    }
});

export default DealsScreen;

