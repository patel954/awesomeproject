import * as React from 'react';


import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import DealsTab from './Tabs/DealsTab';
import SearchTab from './Tabs/SearchTab';
import FlyersTab from './Tabs/FlyersTab';
import PostOfferTab from './Tabs/PostOfferTabs';



const MaterialBottomTabs = createBottomTabNavigator();



export default class App extends React.Component{
  render(){
    return(
      <NavigationContainer>
       <MaterialBottomTabs.Navigator>
          <MaterialBottomTabs.Screen name="Deals" component={DealsTab} />
          <MaterialBottomTabs.Screen name="Flyers" component={FlyersTab} />
          <MaterialBottomTabs.Screen name="Post Offer" component={PostOfferTab} />
          <MaterialBottomTabs.Screen name="Search" component={SearchTab} />
        </MaterialBottomTabs.Navigator>
      </NavigationContainer>
    );
  }
}

