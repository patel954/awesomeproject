import * as React from 'react';

import { createDrawerNavigator } from '@react-navigation/drawer';

import DealsScreen from '../Screens/DealsScreen';
import AccountSettingsScreen from '../Screens/AccountSettingsScreen';
import AppSettingsScreen from '../Screens/AppSettingsScreen';
import SecurityScreen from '../Screens/SecurityScreen';
import WhatsNewScreen from '../Screens/WhatsNewScreen';

const Drawer = createDrawerNavigator();

export default class DealsTab extends React.Component {
    render(){
        return (
          <Drawer.Navigator initialRouteName="DealsScreen" drawerPosition="right">
            <Drawer.Screen name="Deals" component={DealsScreen} />
            <Drawer.Screen name="Account Settings" component={AccountSettingsScreen} />
            <Drawer.Screen name="App Settings" component={AppSettingsScreen} />
            <Drawer.Screen name="Security" component={SecurityScreen} />
            <Drawer.Screen name="What's new" component={WhatsNewScreen} />
          </Drawer.Navigator>
        );
    }
}